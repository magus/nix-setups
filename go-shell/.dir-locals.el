;;; Directory Local Variables            -*- no-byte-compile: t -*-

((nil . ((projectile-project-type . go)))
 (go-mode . ((compile-command . "go build -v ")
             (lsp-go-directory-filters . ["-.go-build"]))))
