{
  inputs = { nixpkgs.url = "github:nixos/nixpkgs"; };

  outputs = { self, nixpkgs }:
    with nixpkgs.legacyPackages.x86_64-linux; {
      devShell.x86_64-linux = mkShell {
        packages = [
          (python312.withPackages (p: [
            # packages (works best without 'layout python3' in .envrc)
            # p.pydantic
            # p.pytest
            # p.pytest-mypy
            # p.pytest-ruff
            # lsp
            p.pylsp-mypy
            p.pylsp-rope
            p.python-lsp-ruff
            p.python-lsp-server
          ]))
        ];
      };
    };
}
