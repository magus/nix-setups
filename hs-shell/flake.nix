{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      with nixpkgs.legacyPackages.${system};
      let
        t = lib.trivial;
        hl = haskell.lib;
        hsPkgs = haskell.packages.ghc96;

        project = hsPkgs.developPackage {
          root = lib.sourceFilesBySuffices ./. [ ".cabal" ".hs" "LICENSE" ];
          name = "proj";

          modifier = (t.flip t.pipe) [
            hl.dontHaddock
            hl.enableStaticLibraries
            hl.justStaticExecutables
            hl.disableLibraryProfiling
            hl.disableExecutableProfiling
          ];
        };
      in {
        devShells.default = hsPkgs.shellFor {
          packages = _: [ project ];
          nativeBuildInputs = with hsPkgs; [
            cabal-gild
            cabal-install
            haskell-language-server
          ];
        };
      });
}
