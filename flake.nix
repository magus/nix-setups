{
  description = "My collection of templates";

  outputs = { self }: {
    templates = {
      clj-shell = {
        path = ./clj-shell;
        description = "Shell setup for Clojure";
      };
      go-shell = {
        path = ./go-shell;
        description = "Shell setup for Go";
      };
      hs-shell = {
        path = ./hs-shell;
        description = "Shell setup for Haskell";
      };
      kt-shell = {
        path = ./kt-shell;
        description = "Shell setup for Kotlin";
      };
      ml-shell = {
        path = ./ml-shell;
        description = "Shell setup for OCaml";
      };
      nodejs-shell = {
        path = ./nodejs-shell;
        description = "Shell setup for NodeJS";
      };
      rs-shell = {
        path = ./rs-shell;
        description = "Shell setup for Rust";
      };
      py-shell = {
        path = ./py-shell;
        description = "Shell setup for Python";
      };
    };
  };
}
