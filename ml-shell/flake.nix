{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-23.11";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      with nixpkgs.legacyPackages.${system}; {
        devShells.default = mkShell { buildInputs = [ ocaml opam zstd ]; };
      });
}

# opam switch create 5.1.0
# opam install dune merlin ocaml-lsp-server odoc ocamlformat utop dune-release
